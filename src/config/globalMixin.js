import { mapState, mapWritableState, mapActions } from "pinia";
import { useProdStore } from "@/store/base";

export default {
  computed: {
    ...mapState(useProdStore, [
      "getUser",
      "getAccount",
      "getPoof",
      "getStatus",
      "getSilentMode",
      "getPhoneNumber",
      "getAccountInfo",
      "storePackages",
    ]),
    ...mapWritableState(useProdStore, [
      "profileDetail",
      "isAppLoading",
      "showPremium",
      "hasFreeDownload",


    ]),
  },
  methods: {
    ...mapActions(useProdStore, ["downloadVideo", "payment",  "goBack","selectedPoofStories"]),
  },
};
