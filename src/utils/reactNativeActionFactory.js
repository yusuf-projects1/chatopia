const callbacks = {};

const actionCreatorCB = (action, value, callback) => {
  if (window.ReactNativeWebView) {
    const messageId =
      Date.now().toString(36) + Math.random().toString(36).substring(2);
    if (callback) {
      callbacks[messageId] = callback;
    }
    window.ReactNativeWebView.postMessage(
      JSON.stringify({ action, value, messageId })
    );
    return true;
  }
  return null;
};

window.receiveMessage = (message) => {
  const { messageId, data } = message;
  const callback = callbacks[messageId];
  if (callback) {
    callback(data);
    delete callbacks[messageId];
  }
};

const actionCreator = (action, value) => {
  if (window.ReactNativeWebView) {
    window.ReactNativeWebView.postMessage(JSON.stringify({ action, value }));
    return true;
  }
  return null;
};

export const actions = {
  openInstagramLogin: () => {
    return actionCreator("openInstagramLogin");
  },
  setStorePackets: (value) => {
    return new Promise((resolve) => {
      actionCreatorCB("setStorePackets", value, resolve);
    });
  },

  purchase: (sku) => {
    return new Promise((resolve) => {
      actionCreatorCB("purchase", sku, resolve);
    });
  },

  reactDebug: (value) => {
    // interstitial
    // rewarded
    // Example:

    return actionCreator("reactDebug", value);
  },

  linkShow: (value) => {
    // interstitial
    // rewarded
    // Example:

    return actionCreator("linkShow", value);
  },

  adShow: (adType) => {
    // interstitial
    // rewarded
    // Example:

    return actionCreator(
      "adShow",
      JSON.stringify({
        adType: adType,
      })
    );
  },

  rate: () => {
    return new Promise((resolve) => {
      actionCreatorCB(
        "rate",
        JSON.stringify({
          AppleAppID: "6461727481",
          GooglePackageName: "com.melo.tikdown",
        }),
        resolve
      );
    });
  },

  request: (payload) => {
    return new Promise((resolve) => {
      actionCreatorCB("request", JSON.stringify(payload), resolve);
    });
  },

  paste: () => {
    return actionCreator("paste");
  },

  store: (value) => {
    return new Promise((resolve) => {
      actionCreatorCB("store", value, resolve);
    });
  },

  share: (payload) => {
    return new Promise((resolve) => {
      actionCreatorCB("share", JSON.stringify(payload), resolve);
    });
  },

  getConfig: () => {
    return new Promise((resolve) => {
      actionCreatorCB("getConfig", null, resolve);
    });
  },

  download: (payload) => {
    return new Promise((resolve) => {
      actionCreatorCB("download", JSON.stringify(payload), resolve);
    });
  },

  imageUrlToBase64: (payload) => {
    return new Promise((resolve) => {
      actionCreatorCB("imageUrlToBase64", JSON.stringify(payload), resolve);
    });
  },
};

export default actions;
