﻿export const countriesWithCodes = [
  {
    code: "+7 840",
    name: "Abkhazia",
  },
  {
    code: "+93",
    name: "Afghanistan",
  },
  {
    code: "+355",
    name: "Albania",
  },
  {
    code: "+213",
    name: "Algeria",
  },
  {
    code: "+1 684",
    name: "American Samoa",
  },
  {
    code: "+376",
    name: "Andorra",
  },
  {
    code: "+244",
    name: "Angola",
  },
  {
    code: "+1 264",
    name: "Anguilla",
  },
  {
    code: "+1 268",
    name: "Antigua and Barbuda",
  },
  {
    code: "+54",
    name: "Argentina",
  },
  {
    code: "+374",
    name: "Armenia",
  },
  {
    code: "+297",
    name: "Aruba",
  },
  {
    code: "+247",
    name: "Ascension",
  },
  {
    code: "+61",
    name: "Australia",
  },
  {
    code: "+672",
    name: "Australian External Territories",
  },
  {
    code: "+43",
    name: "Austria",
  },
  {
    code: "+994",
    name: "Azerbaijan",
  },
  {
    code: "+1 242",
    name: "Bahamas",
  },
  {
    code: "+973",
    name: "Bahrain",
  },
  {
    code: "+880",
    name: "Bangladesh",
  },
  {
    code: "+1 246",
    name: "Barbados",
  },
  {
    code: "+1 268",
    name: "Barbuda",
  },
  {
    code: "+375",
    name: "Belarus",
  },
  {
    code: "+32",
    name: "Belgium",
  },
  {
    code: "+501",
    name: "Belize",
  },
  {
    code: "+229",
    name: "Benin",
  },
  {
    code: "+1 441",
    name: "Bermuda",
  },
  {
    code: "+975",
    name: "Bhutan",
  },
  {
    code: "+591",
    name: "Bolivia",
  },
  {
    code: "+387",
    name: "Bosnia and Herzegovina",
  },
  {
    code: "+267",
    name: "Botswana",
  },
  {
    code: "+55",
    name: "Brazil",
  },
  {
    code: "+246",
    name: "British Indian Ocean Territory",
  },
  {
    code: "+1 284",
    name: "British Virgin Islands",
  },
  {
    code: "+673",
    name: "Brunei",
  },
  {
    code: "+359",
    name: "Bulgaria",
  },
  {
    code: "+226",
    name: "Burkina Faso",
  },
  {
    code: "+257",
    name: "Burundi",
  },
  {
    code: "+855",
    name: "Cambodia",
  },
  {
    code: "+237",
    name: "Cameroon",
  },
  {
    code: "+1",
    name: "Canada",
  },
  {
    code: "+238",
    name: "Cape Verde",
  },
  {
    code: "+ 345",
    name: "Cayman Islands",
  },
  {
    code: "+236",
    name: "Central African Republic",
  },
  {
    code: "+235",
    name: "Chad",
  },
  {
    code: "+56",
    name: "Chile",
  },
  {
    code: "+86",
    name: "China",
  },
  {
    code: "+61",
    name: "Christmas Island",
  },
  {
    code: "+61",
    name: "Cocos-Keeling Islands",
  },
  {
    code: "+57",
    name: "Colombia",
  },
  {
    code: "+269",
    name: "Comoros",
  },
  {
    code: "+242",
    name: "Congo",
  },
  {
    code: "+243",
    name: "Congo, Dem. Rep. of (Zaire)",
  },
  {
    code: "+682",
    name: "Cook Islands",
  },
  {
    code: "+506",
    name: "Costa Rica",
  },
  {
    code: "+385",
    name: "Croatia",
  },
  {
    code: "+53",
    name: "Cuba",
  },
  {
    code: "+599",
    name: "Curacao",
  },
  {
    code: "+537",
    name: "Cyprus",
  },
  {
    code: "+420",
    name: "Czech Republic",
  },
  {
    code: "+45",
    name: "Denmark",
  },
  {
    code: "+246",
    name: "Diego Garcia",
  },
  {
    code: "+253",
    name: "Djibouti",
  },
  {
    code: "+1 767",
    name: "Dominica",
  },
  {
    code: "+1 809",
    name: "Dominican Republic",
  },
  {
    code: "+670",
    name: "East Timor",
  },
  {
    code: "+56",
    name: "Easter Island",
  },
  {
    code: "+593",
    name: "Ecuador",
  },
  {
    code: "+20",
    name: "Egypt",
  },
  {
    code: "+503",
    name: "El Salvador",
  },
  {
    code: "+240",
    name: "Equatorial Guinea",
  },
  {
    code: "+291",
    name: "Eritrea",
  },
  {
    code: "+372",
    name: "Estonia",
  },
  {
    code: "+251",
    name: "Ethiopia",
  },
  {
    code: "+500",
    name: "Falkland Islands",
  },
  {
    code: "+298",
    name: "Faroe Islands",
  },
  {
    code: "+679",
    name: "Fiji",
  },
  {
    code: "+358",
    name: "Finland",
  },
  {
    code: "+33",
    name: "France",
  },
  {
    code: "+596",
    name: "French Antilles",
  },
  {
    code: "+594",
    name: "French Guiana",
  },
  {
    code: "+689",
    name: "French Polynesia",
  },
  {
    code: "+241",
    name: "Gabon",
  },
  {
    code: "+220",
    name: "Gambia",
  },
  {
    code: "+995",
    name: "Georgia",
  },
  {
    code: "+49",
    name: "Germany",
  },
  {
    code: "+233",
    name: "Ghana",
  },
  {
    code: "+350",
    name: "Gibraltar",
  },
  {
    code: "+30",
    name: "Greece",
  },
  {
    code: "+299",
    name: "Greenland",
  },
  {
    code: "+1 473",
    name: "Grenada",
  },
  {
    code: "+590",
    name: "Guadeloupe",
  },
  {
    code: "+1 671",
    name: "Guam",
  },
  {
    code: "+502",
    name: "Guatemala",
  },
  {
    code: "+224",
    name: "Guinea",
  },
  {
    code: "+245",
    name: "Guinea-Bissau",
  },
  {
    code: "+595",
    name: "Guyana",
  },
  {
    code: "+509",
    name: "Haiti",
  },
  {
    code: "+504",
    name: "Honduras",
  },
  {
    code: "+852",
    name: "Hong Kong SAR China",
  },
  {
    code: "+36",
    name: "Hungary",
  },
  {
    code: "+354",
    name: "Iceland",
  },
  {
    code: "+91",
    name: "India",
  },
  {
    code: "+62",
    name: "Indonesia",
  },
  {
    code: "+98",
    name: "Iran",
  },
  {
    code: "+964",
    name: "Iraq",
  },
  {
    code: "+353",
    name: "Ireland",
  },
  {
    code: "+972",
    name: "Israel",
  },
  {
    code: "+39",
    name: "Italy",
  },
  {
    code: "+225",
    name: "Ivory Coast",
  },
  {
    code: "+1 876",
    name: "Jamaica",
  },
  {
    code: "+81",
    name: "Japan",
  },
  {
    code: "+962",
    name: "Jordan",
  },
  {
    code: "+7 7",
    name: "Kazakhstan",
  },
  {
    code: "+254",
    name: "Kenya",
  },
  {
    code: "+686",
    name: "Kiribati",
  },
  {
    code: "+965",
    name: "Kuwait",
  },
  {
    code: "+996",
    name: "Kyrgyzstan",
  },
  {
    code: "+856",
    name: "Laos",
  },
  {
    code: "+371",
    name: "Latvia",
  },
  {
    code: "+961",
    name: "Lebanon",
  },
  {
    code: "+266",
    name: "Lesotho",
  },
  {
    code: "+231",
    name: "Liberia",
  },
  {
    code: "+218",
    name: "Libya",
  },
  {
    code: "+423",
    name: "Liechtenstein",
  },
  {
    code: "+370",
    name: "Lithuania",
  },
  {
    code: "+352",
    name: "Luxembourg",
  },
  {
    code: "+853",
    name: "Macau SAR China",
  },
  {
    code: "+389",
    name: "Macedonia",
  },
  {
    code: "+261",
    name: "Madagascar",
  },
  {
    code: "+265",
    name: "Malawi",
  },
  {
    code: "+60",
    name: "Malaysia",
  },
  {
    code: "+960",
    name: "Maldives",
  },
  {
    code: "+223",
    name: "Mali",
  },
  {
    code: "+356",
    name: "Malta",
  },
  {
    code: "+692",
    name: "Marshall Islands",
  },
  {
    code: "+596",
    name: "Martinique",
  },
  {
    code: "+222",
    name: "Mauritania",
  },
  {
    code: "+230",
    name: "Mauritius",
  },
  {
    code: "+262",
    name: "Mayotte",
  },
  {
    code: "+52",
    name: "Mexico",
  },
  {
    code: "+691",
    name: "Micronesia",
  },
  {
    code: "+1 808",
    name: "Midway Island",
  },
  {
    code: "+373",
    name: "Moldova",
  },
  {
    code: "+377",
    name: "Monaco",
  },
  {
    code: "+976",
    name: "Mongolia",
  },
  {
    code: "+382",
    name: "Montenegro",
  },
  {
    code: "+1664",
    name: "Montserrat",
  },
  {
    code: "+212",
    name: "Morocco",
  },
  {
    code: "+95",
    name: "Myanmar",
  },
  {
    code: "+264",
    name: "Namibia",
  },
  {
    code: "+674",
    name: "Nauru",
  },
  {
    code: "+977",
    name: "Nepal",
  },
  {
    code: "+31",
    name: "Netherlands",
  },
  {
    code: "+599",
    name: "Netherlands Antilles",
  },
  {
    code: "+1 869",
    name: "Nevis",
  },
  {
    code: "+687",
    name: "New Caledonia",
  },
  {
    code: "+64",
    name: "New Zealand",
  },
  {
    code: "+505",
    name: "Nicaragua",
  },
  {
    code: "+227",
    name: "Niger",
  },
  {
    code: "+234",
    name: "Nigeria",
  },
  {
    code: "+683",
    name: "Niue",
  },
  {
    code: "+672",
    name: "Norfolk Island",
  },
  {
    code: "+850",
    name: "North Korea",
  },
  {
    code: "+1 670",
    name: "Northern Mariana Islands",
  },
  {
    code: "+47",
    name: "Norway",
  },
  {
    code: "+968",
    name: "Oman",
  },
  {
    code: "+92",
    name: "Pakistan",
  },
  {
    code: "+680",
    name: "Palau",
  },
  {
    code: "+970",
    name: "Palestinian Territory",
  },
  {
    code: "+507",
    name: "Panama",
  },
  {
    code: "+675",
    name: "Papua New Guinea",
  },
  {
    code: "+595",
    name: "Paraguay",
  },
  {
    code: "+51",
    name: "Peru",
  },
  {
    code: "+63",
    name: "Philippines",
  },
  {
    code: "+48",
    name: "Poland",
  },
  {
    code: "+351",
    name: "Portugal",
  },
  {
    code: "+1 787",
    name: "Puerto Rico",
  },
  {
    code: "+974",
    name: "Qatar",
  },
  {
    code: "+262",
    name: "Reunion",
  },
  {
    code: "+40",
    name: "Romania",
  },
  {
    code: "+7",
    name: "Russia",
  },
  {
    code: "+250",
    name: "Rwanda",
  },
  {
    code: "+685",
    name: "Samoa",
  },
  {
    code: "+378",
    name: "San Marino",
  },
  {
    code: "+966",
    name: "Saudi Arabia",
  },
  {
    code: "+221",
    name: "Senegal",
  },
  {
    code: "+381",
    name: "Serbia",
  },
  {
    code: "+248",
    name: "Seychelles",
  },
  {
    code: "+232",
    name: "Sierra Leone",
  },
  {
    code: "+65",
    name: "Singapore",
  },
  {
    code: "+421",
    name: "Slovakia",
  },
  {
    code: "+386",
    name: "Slovenia",
  },
  {
    code: "+677",
    name: "Solomon Islands",
  },
  {
    code: "+27",
    name: "South Africa",
  },
  {
    code: "+500",
    name: "South Georgia and the South Sandwich Islands",
  },
  {
    code: "+82",
    name: "South Korea",
  },
  {
    code: "+34",
    name: "Spain",
  },
  {
    code: "+94",
    name: "Sri Lanka",
  },
  {
    code: "+249",
    name: "Sudan",
  },
  {
    code: "+597",
    name: "Suriname",
  },
  {
    code: "+268",
    name: "Swaziland",
  },
  {
    code: "+46",
    name: "Sweden",
  },
  {
    code: "+41",
    name: "Switzerland",
  },
  {
    code: "+963",
    name: "Syria",
  },
  {
    code: "+886",
    name: "Taiwan",
  },
  {
    code: "+992",
    name: "Tajikistan",
  },
  {
    code: "+255",
    name: "Tanzania",
  },
  {
    code: "+66",
    name: "Thailand",
  },
  {
    code: "+670",
    name: "Timor Leste",
  },
  {
    code: "+228",
    name: "Togo",
  },
  {
    code: "+690",
    name: "Tokelau",
  },
  {
    code: "+676",
    name: "Tonga",
  },
  {
    code: "+1 868",
    name: "Trinidad and Tobago",
  },
  {
    code: "+216",
    name: "Tunisia",
  },
  {
    code: "+90",
    name: "Turkey",
  },
  {
    code: "+993",
    name: "Turkmenistan",
  },
  {
    code: "+1 649",
    name: "Turks and Caicos Islands",
  },
  {
    code: "+688",
    name: "Tuvalu",
  },
  {
    code: "+1 340",
    name: "U.S. Virgin Islands",
  },
  {
    code: "+256",
    name: "Uganda",
  },
  {
    code: "+380",
    name: "Ukraine",
  },
  {
    code: "+971",
    name: "United Arab Emirates",
  },
  {
    code: "+44",
    name: "United Kingdom",
  },
  {
    code: "+1",
    name: "United States",
  },
  {
    code: "+598",
    name: "Uruguay",
  },
  {
    code: "+998",
    name: "Uzbekistan",
  },
  {
    code: "+678",
    name: "Vanuatu",
  },
  {
    code: "+58",
    name: "Venezuela",
  },
  {
    code: "+84",
    name: "Vietnam",
  },
  {
    code: "+1 808",
    name: "Wake Island",
  },
  {
    code: "+681",
    name: "Wallis and Futuna",
  },
  {
    code: "+967",
    name: "Yemen",
  },
  {
    code: "+260",
    name: "Zambia",
  },
  {
    code: "+255",
    name: "Zanzibar",
  },
  {
    code: "+263",
    name: "Zimbabwe",
  },
];
export const countriesWithFlags = [
  { country: "Afghanistan", flag: "🇦🇫" },
  { country: "Albania", flag: "🇦🇱" },
  { country: "Algeria", flag: "🇩🇿" },
  { country: "Andorra", flag: "🇦🇩" },
  { country: "Angola", flag: "🇦🇴" },
  { country: "Antigua & Barbuda", flag: "🇦🇬" },
  { country: "Argentina", flag: "🇦🇷" },
  { country: "Armenia", flag: "🇦🇲" },
  { country: "Australia", flag: "🇦🇺" },
  { country: "Austria", flag: "🇦🇹" },
  { country: "Azerbaijan", flag: "🇦🇿" },
  { country: "Bahamas", flag: "🇧🇸" },
  { country: "Bahrain", flag: "🇧🇭" },
  { country: "Bangladesh", flag: "🇧🇩" },
  { country: "Barbados", flag: "🇧🇧" },
  { country: "Belarus", flag: "🇧🇾" },
  { country: "Belgium", flag: "🇧🇪" },
  { country: "Belize", flag: "🇧🇿" },
  { country: "Benin", flag: "🇧🇯" },
  { country: "Bhutan", flag: "🇧🇹" },
  { country: "Bolivia", flag: "🇧🇴" },
  { country: "Bosnia & Herzegovina", flag: "🇧🇦" },
  { country: "Botswana", flag: "🇧🇼" },
  { country: "Brazil", flag: "🇧🇷" },
  { country: "Brunei", flag: "🇧🇳" },
  { country: "Bulgaria", flag: "🇧🇬" },
  { country: "Burkina Faso", flag: "🇧🇫" },
  { country: "Burundi", flag: "🇧🇮" },
  { country: "Cabo Verde", flag: "🇨🇻" },
  { country: "Cambodia", flag: "🇰🇭" },
  { country: "Cameroon", flag: "🇨🇲" },
  { country: "Canada", flag: "🇨🇦" },
  { country: "Central African Republic", flag: "🇨🇫" },
  { country: "Chad", flag: "🇹🇩" },
  { country: "Chile", flag: "🇨🇱" },
  { country: "China", flag: "🇨🇳" },
  { country: "Colombia", flag: "🇨🇴" },
  { country: "Comoros", flag: "🇰🇲" },
  { country: "Congo", flag: "🇨🇬" },
  { country: "Costa Rica", flag: "🇨🇷" },
  { country: "Croatia", flag: "🇭🇷" },
  { country: "Cuba", flag: "🇨🇺" },
  { country: "Cyprus", flag: "🇨🇾" },
  { country: "Czech Republic", flag: "🇨🇿" },
  { country: "Denmark", flag: "🇩🇰" },
  { country: "Djibouti", flag: "🇩🇯" },
  { country: "Dominica", flag: "🇩🇲" },
  { country: "Dominican Republic", flag: "🇩🇴" },
  { country: "Ecuador", flag: "🇪🇨" },
  { country: "Egypt", flag: "🇪🇬" },
  { country: "El Salvador", flag: "🇸🇻" },
  { country: "Equatorial Guinea", flag: "🇬🇶" },
  { country: "Eritrea", flag: "🇪🇷" },
  { country: "Estonia", flag: "🇪🇪" },
  { country: "Eswatini", flag: "🇸🇿" },
  { country: "Ethiopia", flag: "🇪🇹" },
  { country: "Fiji", flag: "🇫🇯" },
  { country: "Finland", flag: "🇫🇮" },
  { country: "France", flag: "🇫🇷" },
  { country: "Gabon", flag: "🇬🇦" },
  { country: "Gambia", flag: "🇬🇲" },
  { country: "Georgia", flag: "🇬🇪" },
  { country: "Germany", flag: "🇩🇪" },
  { country: "Ghana", flag: "🇬🇭" },
  { country: "Greece", flag: "🇬🇷" },
  { country: "Grenada", flag: "🇬🇩" },
  { country: "Guatemala", flag: "🇬🇹" },
  { country: "Guinea", flag: "🇬🇳" },
  { country: "Guinea-Bissau", flag: "🇬🇼" },
  { country: "Guyana", flag: "🇬🇾" },
  { country: "Haiti", flag: "🇭🇹" },
  { country: "Honduras", flag: "🇭🇳" },
  { country: "Hungary", flag: "🇭🇺" },
  { country: "Iceland", flag: "🇮🇸" },
  { country: "India", flag: "🇮🇳" },
  { country: "Indonesia", flag: "🇮🇩" },
  { country: "Iran", flag: "🇮🇷" },
  { country: "Iraq", flag: "🇮🇶" },
  { country: "Ireland", flag: "🇮🇪" },
  { country: "Israel", flag: "🇮🇱" },
  { country: "Italy", flag: "🇮🇹" },
  { country: "Jamaica", flag: "🇯🇲" },
  { country: "Japan", flag: "🇯🇵" },
  { country: "Jordan", flag: "🇯🇴" },
  { country: "Kazakhstan", flag: "🇰🇿" },
  { country: "Kenya", flag: "🇰🇪" },
  { country: "Kiribati", flag: "🇰🇮" },
  { country: "Kuwait", flag: "🇰🇼" },
  { country: "Kyrgyzstan", flag: "🇰🇬" },
  { country: "Laos", flag: "🇱🇦" },
  { country: "Latvia", flag: "🇱🇻" },
  { country: "Lebanon", flag: "🇱🇧" },
  { country: "Lesotho", flag: "🇱🇸" },
  { country: "Liberia", flag: "🇱🇷" },
  { country: "Libya", flag: "🇱🇾" },
  { country: "Liechtenstein", flag: "🇱🇮" },
  { country: "Lithuania", flag: "🇱🇹" },
  { country: "Luxembourg", flag: "🇱🇺" },
  { country: "Madagascar", flag: "🇲🇬" },
  { country: "Malawi", flag: "🇲🇼" },
  { country: "Malaysia", flag: "🇲🇾" },
  { country: "Maldives", flag: "🇲🇻" },
  { country: "Mali", flag: "🇲🇱" },
  { country: "Malta", flag: "🇲🇹" },
  { country: "Marshall Islands", flag: "🇲🇭" },
  { country: "Mauritania", flag: "🇲🇷" },
  { country: "Mauritius", flag: "🇲🇺" },
  { country: "Mexico", flag: "🇲🇽" },
  { country: "Micronesia", flag: "🇫🇲" },
  { country: "Moldova", flag: "🇲🇩" },
  { country: "Monaco", flag: "🇲🇨" },
  { country: "Mongolia", flag: "🇲🇳" },
  { country: "Montenegro", flag: "🇲🇪" },
  { country: "Morocco", flag: "🇲🇦" },
  { country: "Mozambique", flag: "🇲🇿" },
  { country: "Myanmar", flag: "🇲🇲" },
  { country: "Namibia", flag: "🇳🇦" },
  { country: "Nauru", flag: "🇳🇷" },
  { country: "Nepal", flag: "🇳🇵" },
  { country: "Netherlands", flag: "🇳🇱" },
  { country: "New Zealand", flag: "🇳🇿" },
  { country: "Nicaragua", flag: "🇳🇮" },
  { country: "Niger", flag: "🇳🇪" },
  { country: "Nigeria", flag: "🇳🇬" },
  { country: "North Korea", flag: "🇰🇵" },
  { country: "North Macedonia", flag: "🇲🇰" },
  { country: "Norway", flag: "🇳🇴" },
  { country: "Oman", flag: "🇴🇲" },
  { country: "Pakistan", flag: "🇵🇰" },
  { country: "Palau", flag: "🇵🇼" },
  { country: "Palestine", flag: "🇵🇸" },
  { country: "Panama", flag: "🇵🇦" },
  { country: "Papua New Guinea", flag: "🇵🇬" },
  { country: "Paraguay", flag: "🇵🇾" },
  { country: "Peru", flag: "🇵🇪" },
  { country: "Philippines", flag: "🇵🇭" },
  { country: "Poland", flag: "🇵🇱" },
  { country: "Portugal", flag: "🇵🇹" },
  { country: "Qatar", flag: "🇶🇦" },
  { country: "Romania", flag: "🇷🇴" },
  { country: "Russia", flag: "🇷🇺" },
  { country: "Rwanda", flag: "🇷🇼" },
  { country: "Saint Kitts & Nevis", flag: "🇰🇳" },
  { country: "Saint Lucia", flag: "🇱🇨" },
  { country: "Saint Vincent & Grenadines", flag: "🇻🇨" },
  { country: "Samoa", flag: "🇼🇸" },
  { country: "San Marino", flag: "🇸🇲" },
  { country: "Sao Tome & Principe", flag: "🇸🇹" },
  { country: "Saudi Arabia", flag: "🇸🇦" },
  { country: "Senegal", flag: "🇸🇳" },
  { country: "Serbia", flag: "🇷🇸" },
  { country: "Seychelles", flag: "🇸🇨" },
  { country: "Sierra Leone", flag: "🇸🇱" },
  { country: "Singapore", flag: "🇸🇬" },
  { country: "Slovakia", flag: "🇸🇰" },
  { country: "Slovenia", flag: "🇸🇮" },
  { country: "Solomon Islands", flag: "🇸🇧" },
  { country: "Somalia", flag: "🇸🇴" },
  { country: "South Africa", flag: "🇿🇦" },
  { country: "South Korea", flag: "🇰🇷" },
  { country: "South Sudan", flag: "🇸🇸" },
  { country: "Spain", flag: "🇪🇸" },
  { country: "Sri Lanka", flag: "🇱🇰" },
  { country: "Sudan", flag: "🇸🇩" },
  { country: "Suriname", flag: "🇸🇷" },
  { country: "Sweden", flag: "🇸🇪" },
  { country: "Switzerland", flag: "🇨🇭" },
  { country: "Syria", flag: "🇸🇾" },
  { country: "Tajikistan", flag: "🇹🇯" },
  { country: "Tanzania", flag: "🇹🇿" },
  { country: "Thailand", flag: "🇹🇭" },
  { country: "Timor-Leste", flag: "🇹🇱" },
  { country: "Togo", flag: "🇹🇬" },
  { country: "Tonga", flag: "🇹🇴" },
  { country: "Trinidad & Tobago", flag: "🇹🇹" },
  { country: "Tunisia", flag: "🇹🇳" },
  { country: "Turkey", flag: "🇹🇷" },
  { country: "Turkmenistan", flag: "🇹🇲" },
  { country: "Tuvalu", flag: "🇹🇻" },
  { country: "Uganda", flag: "🇺🇬" },
  { country: "Ukraine", flag: "🇺🇦" },
  { country: "United Arab Emirates", flag: "🇦🇪" },
  { country: "United Kingdom", flag: "🇬🇧" },
  { country: "United States", flag: "🇺🇸" },
  { country: "Uruguay", flag: "🇺🇾" },
  { country: "Uzbekistan", flag: "🇺🇿" },
  { country: "Vanuatu", flag: "🇻🇺" },
  { country: "Vatican City", flag: "🇻🇦" },
  { country: "Venezuela", flag: "🇻🇪" },
  { country: "Vietnam", flag: "🇻🇳" },
  { country: "Yemen", flag: "🇾🇪" },
  { country: "Zambia", flag: "🇿🇲" },
  { country: "Zimbabwe", flag: "🇿🇼" },
];

export const countries = [
  {
    country: "Afghanistan",
    code: "+93",
    flag: "🇦🇫",
  },
  {
    country: "Albania",
    code: "+355",
    flag: "🇦🇱",
  },
  {
    country: "Algeria",
    code: "+213",
    flag: "🇩🇿",
  },
  {
    country: "Andorra",
    code: "+376",
    flag: "🇦🇩",
  },
  {
    country: "Angola",
    code: "+244",
    flag: "🇦🇴",
  },
  {
    country: "Argentina",
    code: "+54",
    flag: "🇦🇷",
  },
  {
    country: "Armenia",
    code: "+374",
    flag: "🇦🇲",
  },
  {
    country: "Australia",
    code: "+61",
    flag: "🇦🇺",
  },
  {
    country: "Austria",
    code: "+43",
    flag: "🇦🇹",
  },
  {
    country: "Azerbaijan",
    code: "+994",
    flag: "🇦🇿",
  },
  {
    country: "Bahamas",
    code: "+1 242",
    flag: "🇧🇸",
  },
  {
    country: "Bahrain",
    code: "+973",
    flag: "🇧🇭",
  },
  {
    country: "Bangladesh",
    code: "+880",
    flag: "🇧🇩",
  },
  {
    country: "Barbados",
    code: "+1 246",
    flag: "🇧🇧",
  },
  {
    country: "Belarus",
    code: "+375",
    flag: "🇧🇾",
  },
  {
    country: "Belgium",
    code: "+32",
    flag: "🇧🇪",
  },
  {
    country: "Belize",
    code: "+501",
    flag: "🇧🇿",
  },
  {
    country: "Benin",
    code: "+229",
    flag: "🇧🇯",
  },
  {
    country: "Bhutan",
    code: "+975",
    flag: "🇧🇹",
  },
  {
    country: "Bolivia",
    code: "+591",
    flag: "🇧🇴",
  },
  {
    country: "Botswana",
    code: "+267",
    flag: "🇧🇼",
  },
  {
    country: "Brazil",
    code: "+55",
    flag: "🇧🇷",
  },
  {
    country: "Brunei",
    code: "+673",
    flag: "🇧🇳",
  },
  {
    country: "Bulgaria",
    code: "+359",
    flag: "🇧🇬",
  },
  {
    country: "Burkina Faso",
    code: "+226",
    flag: "🇧🇫",
  },
  {
    country: "Burundi",
    code: "+257",
    flag: "🇧🇮",
  },
  {
    country: "Cambodia",
    code: "+855",
    flag: "🇰🇭",
  },
  {
    country: "Cameroon",
    code: "+237",
    flag: "🇨🇲",
  },
  {
    country: "Canada",
    code: "+1",
    flag: "🇨🇦",
  },
  {
    country: "Central African Republic",
    code: "+236",
    flag: "🇨🇫",
  },
  {
    country: "Chad",
    code: "+235",
    flag: "🇹🇩",
  },
  {
    country: "Chile",
    code: "+56",
    flag: "🇨🇱",
  },
  {
    country: "China",
    code: "+86",
    flag: "🇨🇳",
  },
  {
    country: "Colombia",
    code: "+57",
    flag: "🇨🇴",
  },
  {
    country: "Comoros",
    code: "+269",
    flag: "🇰🇲",
  },
  {
    country: "Congo",
    code: "+242",
    flag: "🇨🇬",
  },
  {
    country: "Costa Rica",
    code: "+506",
    flag: "🇨🇷",
  },
  {
    country: "Croatia",
    code: "+385",
    flag: "🇭🇷",
  },
  {
    country: "Cuba",
    code: "+53",
    flag: "🇨🇺",
  },
  {
    country: "Cyprus",
    code: "+537",
    flag: "🇨🇾",
  },
  {
    country: "Czech Republic",
    code: "+420",
    flag: "🇨🇿",
  },
  {
    country: "Denmark",
    code: "+45",
    flag: "🇩🇰",
  },
  {
    country: "Djibouti",
    code: "+253",
    flag: "🇩🇯",
  },
  {
    country: "Dominica",
    code: "+1 767",
    flag: "🇩🇲",
  },
  {
    country: "Dominican Republic",
    code: "+1 809",
    flag: "🇩🇴",
  },
  {
    country: "Ecuador",
    code: "+593",
    flag: "🇪🇨",
  },
  {
    country: "Egypt",
    code: "+20",
    flag: "🇪🇬",
  },
  {
    country: "El Salvador",
    code: "+503",
    flag: "🇸🇻",
  },
  {
    country: "Equatorial Guinea",
    code: "+240",
    flag: "🇬🇶",
  },
  {
    country: "Eritrea",
    code: "+291",
    flag: "🇪🇷",
  },
  {
    country: "Estonia",
    code: "+372",
    flag: "🇪🇪",
  },
  {
    country: "Ethiopia",
    code: "+251",
    flag: "🇪🇹",
  },
  {
    country: "Fiji",
    code: "+679",
    flag: "🇫🇯",
  },
  {
    country: "Finland",
    code: "+358",
    flag: "🇫🇮",
  },
  {
    country: "France",
    code: "+33",
    flag: "🇫🇷",
  },
  {
    country: "Gabon",
    code: "+241",
    flag: "🇬🇦",
  },
  {
    country: "Gambia",
    code: "+220",
    flag: "🇬🇲",
  },
  {
    country: "Georgia",
    code: "+995",
    flag: "🇬🇪",
  },
  {
    country: "Germany",
    code: "+49",
    flag: "🇩🇪",
  },
  {
    country: "Ghana",
    code: "+233",
    flag: "🇬🇭",
  },
  {
    country: "Greece",
    code: "+30",
    flag: "🇬🇷",
  },
  {
    country: "Grenada",
    code: "+1 473",
    flag: "🇬🇩",
  },
  {
    country: "Guatemala",
    code: "+502",
    flag: "🇬🇹",
  },
  {
    country: "Guinea",
    code: "+224",
    flag: "🇬🇳",
  },
  {
    country: "Guinea-Bissau",
    code: "+245",
    flag: "🇬🇼",
  },
  {
    country: "Guyana",
    code: "+595",
    flag: "🇬🇾",
  },
  {
    country: "Haiti",
    code: "+509",
    flag: "🇭🇹",
  },
  {
    country: "Honduras",
    code: "+504",
    flag: "🇭🇳",
  },
  {
    country: "Hungary",
    code: "+36",
    flag: "🇭🇺",
  },
  {
    country: "Iceland",
    code: "+354",
    flag: "🇮🇸",
  },
  {
    country: "India",
    code: "+91",
    flag: "🇮🇳",
  },
  {
    country: "Indonesia",
    code: "+62",
    flag: "🇮🇩",
  },
  {
    country: "Iran",
    code: "+98",
    flag: "🇮🇷",
  },
  {
    country: "Iraq",
    code: "+964",
    flag: "🇮🇶",
  },
  {
    country: "Ireland",
    code: "+353",
    flag: "🇮🇪",
  },
  {
    country: "Israel",
    code: "+972",
    flag: "🇮🇱",
  },
  {
    country: "Italy",
    code: "+39",
    flag: "🇮🇹",
  },
  {
    country: "Jamaica",
    code: "+1 876",
    flag: "🇯🇲",
  },
  {
    country: "Japan",
    code: "+81",
    flag: "🇯🇵",
  },
  {
    country: "Jordan",
    code: "+962",
    flag: "🇯🇴",
  },
  {
    country: "Kazakhstan",
    code: "+7 7",
    flag: "🇰🇿",
  },
  {
    country: "Kenya",
    code: "+254",
    flag: "🇰🇪",
  },
  {
    country: "Kiribati",
    code: "+686",
    flag: "🇰🇮",
  },
  {
    country: "Kuwait",
    code: "+965",
    flag: "🇰🇼",
  },
  {
    country: "Kyrgyzstan",
    code: "+996",
    flag: "🇰🇬",
  },
  {
    country: "Laos",
    code: "+856",
    flag: "🇱🇦",
  },
  {
    country: "Latvia",
    code: "+371",
    flag: "🇱🇻",
  },
  {
    country: "Lebanon",
    code: "+961",
    flag: "🇱🇧",
  },
  {
    country: "Lesotho",
    code: "+266",
    flag: "🇱🇸",
  },
  {
    country: "Liberia",
    code: "+231",
    flag: "🇱🇷",
  },
  {
    country: "Libya",
    code: "+218",
    flag: "🇱🇾",
  },
  {
    country: "Liechtenstein",
    code: "+423",
    flag: "🇱🇮",
  },
  {
    country: "Lithuania",
    code: "+370",
    flag: "🇱🇹",
  },
  {
    country: "Luxembourg",
    code: "+352",
    flag: "🇱🇺",
  },
  {
    country: "Madagascar",
    code: "+261",
    flag: "🇲🇬",
  },
  {
    country: "Malawi",
    code: "+265",
    flag: "🇲🇼",
  },
  {
    country: "Malaysia",
    code: "+60",
    flag: "🇲🇾",
  },
  {
    country: "Maldives",
    code: "+960",
    flag: "🇲🇻",
  },
  {
    country: "Mali",
    code: "+223",
    flag: "🇲🇱",
  },
  {
    country: "Malta",
    code: "+356",
    flag: "🇲🇹",
  },
  {
    country: "Marshall Islands",
    code: "+692",
    flag: "🇲🇭",
  },
  {
    country: "Mauritania",
    code: "+222",
    flag: "🇲🇷",
  },
  {
    country: "Mauritius",
    code: "+230",
    flag: "🇲🇺",
  },
  {
    country: "Mexico",
    code: "+52",
    flag: "🇲🇽",
  },
  {
    country: "Micronesia",
    code: "+691",
    flag: "🇫🇲",
  },
  {
    country: "Moldova",
    code: "+373",
    flag: "🇲🇩",
  },
  {
    country: "Monaco",
    code: "+377",
    flag: "🇲🇨",
  },
  {
    country: "Mongolia",
    code: "+976",
    flag: "🇲🇳",
  },
  {
    country: "Montenegro",
    code: "+382",
    flag: "🇲🇪",
  },
  {
    country: "Morocco",
    code: "+212",
    flag: "🇲🇦",
  },
  {
    country: "Myanmar",
    code: "+95",
    flag: "🇲🇲",
  },
  {
    country: "Namibia",
    code: "+264",
    flag: "🇳🇦",
  },
  {
    country: "Nauru",
    code: "+674",
    flag: "🇳🇷",
  },
  {
    country: "Nepal",
    code: "+977",
    flag: "🇳🇵",
  },
  {
    country: "Netherlands",
    code: "+31",
    flag: "🇳🇱",
  },
  {
    country: "New Zealand",
    code: "+64",
    flag: "🇳🇿",
  },
  {
    country: "Nicaragua",
    code: "+505",
    flag: "🇳🇮",
  },
  {
    country: "Niger",
    code: "+227",
    flag: "🇳🇪",
  },
  {
    country: "Nigeria",
    code: "+234",
    flag: "🇳🇬",
  },
  {
    country: "North Korea",
    code: "+850",
    flag: "🇰🇵",
  },
  {
    country: "Norway",
    code: "+47",
    flag: "🇳🇴",
  },
  {
    country: "Oman",
    code: "+968",
    flag: "🇴🇲",
  },
  {
    country: "Pakistan",
    code: "+92",
    flag: "🇵🇰",
  },
  {
    country: "Palau",
    code: "+680",
    flag: "🇵🇼",
  },
  {
    country: "Panama",
    code: "+507",
    flag: "🇵🇦",
  },
  {
    country: "Papua New Guinea",
    code: "+675",
    flag: "🇵🇬",
  },
  {
    country: "Paraguay",
    code: "+595",
    flag: "🇵🇾",
  },
  {
    country: "Peru",
    code: "+51",
    flag: "🇵🇪",
  },
  {
    country: "Philippines",
    code: "+63",
    flag: "🇵🇭",
  },
  {
    country: "Poland",
    code: "+48",
    flag: "🇵🇱",
  },
  {
    country: "Portugal",
    code: "+351",
    flag: "🇵🇹",
  },
  {
    country: "Qatar",
    code: "+974",
    flag: "🇶🇦",
  },
  {
    country: "Romania",
    code: "+40",
    flag: "🇷🇴",
  },
  {
    country: "Russia",
    code: "+7",
    flag: "🇷🇺",
  },
  {
    country: "Rwanda",
    code: "+250",
    flag: "🇷🇼",
  },
  {
    country: "Samoa",
    code: "+685",
    flag: "🇼🇸",
  },
  {
    country: "San Marino",
    code: "+378",
    flag: "🇸🇲",
  },
  {
    country: "Saudi Arabia",
    code: "+966",
    flag: "🇸🇦",
  },
  {
    country: "Senegal",
    code: "+221",
    flag: "🇸🇳",
  },
  {
    country: "Serbia",
    code: "+381",
    flag: "🇷🇸",
  },
  {
    country: "Seychelles",
    code: "+248",
    flag: "🇸🇨",
  },
  {
    country: "Sierra Leone",
    code: "+232",
    flag: "🇸🇱",
  },
  {
    country: "Singapore",
    code: "+65",
    flag: "🇸🇬",
  },
  {
    country: "Slovakia",
    code: "+421",
    flag: "🇸🇰",
  },
  {
    country: "Slovenia",
    code: "+386",
    flag: "🇸🇮",
  },
  {
    country: "Solomon Islands",
    code: "+677",
    flag: "🇸🇧",
  },
  {
    country: "South Africa",
    code: "+27",
    flag: "🇿🇦",
  },
  {
    country: "South Korea",
    code: "+82",
    flag: "🇰🇷",
  },
  {
    country: "Spain",
    code: "+34",
    flag: "🇪🇸",
  },
  {
    country: "Sri Lanka",
    code: "+94",
    flag: "🇱🇰",
  },
  {
    country: "Sudan",
    code: "+249",
    flag: "🇸🇩",
  },
  {
    country: "Suriname",
    code: "+597",
    flag: "🇸🇷",
  },
  {
    country: "Sweden",
    code: "+46",
    flag: "🇸🇪",
  },
  {
    country: "Switzerland",
    code: "+41",
    flag: "🇨🇭",
  },
  {
    country: "Syria",
    code: "+963",
    flag: "🇸🇾",
  },
  {
    country: "Tajikistan",
    code: "+992",
    flag: "🇹🇯",
  },
  {
    country: "Tanzania",
    code: "+255",
    flag: "🇹🇿",
  },
  {
    country: "Thailand",
    code: "+66",
    flag: "🇹🇭",
  },
  {
    country: "Togo",
    code: "+228",
    flag: "🇹🇬",
  },
  {
    country: "Tonga",
    code: "+676",
    flag: "🇹🇴",
  },
  {
    country: "Tunisia",
    code: "+216",
    flag: "🇹🇳",
  },
  {
    country: "Turkey",
    code: "+90",
    flag: "🇹🇷",
  },
  {
    country: "Turkmenistan",
    code: "+993",
    flag: "🇹🇲",
  },
  {
    country: "Tuvalu",
    code: "+688",
    flag: "🇹🇻",
  },
  {
    country: "Uganda",
    code: "+256",
    flag: "🇺🇬",
  },
  {
    country: "Ukraine",
    code: "+380",
    flag: "🇺🇦",
  },
  {
    country: "United Arab Emirates",
    code: "+971",
    flag: "🇦🇪",
  },
  {
    country: "United Kingdom",
    code: "+44",
    flag: "🇬🇧",
  },
  {
    country: "United States",
    code: "+1",
    flag: "🇺🇸",
  },
  {
    country: "Uruguay",
    code: "+598",
    flag: "🇺🇾",
  },
  {
    country: "Uzbekistan",
    code: "+998",
    flag: "🇺🇿",
  },
  {
    country: "Vanuatu",
    code: "+678",
    flag: "🇻🇺",
  },
  {
    country: "Venezuela",
    code: "+58",
    flag: "🇻🇪",
  },
  {
    country: "Vietnam",
    code: "+84",
    flag: "🇻🇳",
  },
  {
    country: "Yemen",
    code: "+967",
    flag: "🇾🇪",
  },
  {
    country: "Zambia",
    code: "+260",
    flag: "🇿🇲",
  },
  {
    country: "Zimbabwe",
    code: "+263",
    flag: "🇿🇼",
  },
];

export default { countries, countriesWithFlags, countriesWithCodes };
