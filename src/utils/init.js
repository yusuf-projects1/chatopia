﻿import { deviceData, iosStorePackages } from "@/utils/dummyData";
import { app } from "@/main";
import { useProdStore } from "@/store/base";

class Base {
  constructor() {
    this.isTestMode = !window.ReactNativeWebView;
    this.reactNativeAction = app.config.globalProperties.$reactNative;
    this.deviceInfo = null;
  }
  async init() {
    await this.setConfig();
    await this.setStorePackages();
    await this.connect();
  }
  async setConfig() {
    if (this.isTestMode) {
      this.deviceInfo = deviceData;
      this.deviceInfo.platform = this.deviceInfo.platform.toLowerCase();
    }
    if (!this.isTestMode) {
      this.deviceInfo = await this.reactNativeAction.getConfig();
      this.deviceInfo.platform = this.deviceInfo.platform.toLowerCase();
    }
    useProdStore().deviceInfo = this.deviceInfo;
  }
  async connect() {
    const connectData = await useProdStore().connect();
  }



  async setStorePackages() {
    const dummyStorePackages = useProdStore().storePackages;
    let mappedStorePackages = dummyStorePackages.map((item) => {
      item.sku = useProdStore().isAndroid ? item.android_sku : item.sku;
      return item;
    });
    useProdStore().storePackages = mappedStorePackages;
    if (this.isTestMode) {
      if (!useProdStore().isAndroid) {
        mappedStorePackages = mappedStorePackages.map((item) => {
          const getItem = iosStorePackages.find((productItem) => {
            return productItem.sku == item.sku;
          });
          item.price = getItem?.price;
          item.localizedPrice = getItem?.localizedPrice;
          item.currency = getItem?.currency;
          item.countryCode = getItem?.countryCode;
          item.freeTrialDay = getItem?.introductoryPriceAsAmountIOS;
          return item;
        });
      }
      useProdStore().storePackages = mappedStorePackages;
    }
    if (!this.isTestMode) {
      this.reactNativeAction
        .setStorePackets(mappedStorePackages)
        .then((res) => {
          this.$reactNative.reactDebug(res);
          var prodArray = [];
          mappedStorePackages.map((productItem) => {
            const getItem = res.find((item) => {
              return item.productId === productItem.sku;
            });
            if (!useProdStore().isAndroid) {
              productItem.price = getItem?.price;
              productItem.title = getItem?.title;
              productItem.localizedPrice = getItem?.localizedPrice;
              productItem.currency = getItem?.currency;
              productItem.countryCode = getItem?.countryCode;
              productItem.freeTrialDay = getItem?.introductoryPriceAsAmountIOS;
            }
            prodArray.push(productItem);
            useProdStore().storePackages = prodArray;
            return productItem;
          });
        });
    }
  }
}

export default Base;
