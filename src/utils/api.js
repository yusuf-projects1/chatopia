import axios from "axios";
import router from "../router/index";
// import { useProdStore } from "@/store/prod";
const ctAxios = axios.create({
  baseURL: "https://chatstory.xlive.website/",
  timeout: 30000,
});

ctAxios.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    //router.push({ name: "home" });
    return Promise.reject(error);
  }
);
const api = {
  connectUser(payload) {
    return ctAxios.post("/api/user/connect", payload);
  },

  connectDevice(payload) {
    return ctAxios.post("/api/user/connect-device", payload);
  },

  unlink(payload) {
    return ctAxios.post("/api/user/unlink", payload);
  },
  rate(payload) {
    return ctAxios.post("/api/user/rate", payload);
  },
  searchUser(payload) {
    return ctAxios.post("/search", payload);
  },
  payment(payload) {
    return ctAxios.post("/api/user/payment", payload);
  },
  downloadVideo(payload) {
    return ctAxios.post("/api/user/download", payload);
  },
};
export default api;
