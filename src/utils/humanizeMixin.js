export function useHumanizeDate() {
    const humanizeDate = (timestamp) => {
        const now = new Date();
        const date = new Date(timestamp * 1000); // Tarihleri milisaniyeye çevir.
        const diffInSeconds = Math.floor((now - date) / 1000);
        const dayInSeconds = 86400;
        const weekInSeconds = 604800;
        const yearInSeconds = 31536000;

        if (diffInSeconds < dayInSeconds) {
            return 'Today';
        } else if (diffInSeconds < dayInSeconds * 2) {
            return 'Yesterday';
        } else if (diffInSeconds < weekInSeconds) {
            return `${Math.floor(diffInSeconds / dayInSeconds)} Days Ago`;
        } else if (diffInSeconds < yearInSeconds) {
            return `${Math.floor(diffInSeconds / weekInSeconds)} Weeks Ago`;
        } else {
            return `${Math.floor(diffInSeconds / yearInSeconds)} Years Ago`;
        }
    };

    return { humanizeDate };
}
