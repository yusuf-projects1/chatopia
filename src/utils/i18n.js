import { createI18n } from "vue-i18n";

const messages = {
  fr: {
    Articles: "Anasaya",
    Academy: "Anasaya",
    min: "Anasaya",
    Warning: "Anasaya",
    Cancel: "Anasaya",
    No: "Anasaya",
    Ideas: "Anasaya",
    Tags: "Anasaya",
    Scenario: "Anasaya",
    Categories: "Anasaya",
    "Welcome to TikRun": "Anasaya",
    "AI Tag Generator": "Anasaya",
    "Grow your account with our AI tool!": "Anasaya",
    "Let's start!": "Anasaya",
    "Growth your account": "Anasaya",
    "Feel free to select the academy category that best suits your preferences, enhancing your social media journey.":
      "Anasaya",
    "By opting for any of the titles listed below, you can receive assistance in growing your account!":
      "Anasaya",
    "Copy to Clipboard": "Anasaya",
    "AI Content Idea": "Anasaya",
    "Generate Idea": "Anasaya",
    "Enter at least 3 parameters close to your dream content idea.": "Anasaya",
    "AI Tags": "Anasaya",
    "Utilize the power of artificial intelligence to receive tag suggestions for your video content.":
      "Anasaya",
    "Generate Tags": "Anasaya",
    "Best Sounds": "Anasaya",
    "Generated AI Idea": "Anasaya",
    "Generated AI Tags": "Anasaya",
    "Assistant Tools": "Anasaya",

    "Obtain tag recommendations for your video content through the implementation of artificial intelligence.":
      "Anasaya",
    "AI Content Idea Generator": "Anasaya",
    "Get help from artificial intelligence for your video content.": "Anasaya",

    "No history yet!": "Anasaya",
    "There is no content in your AI history.": "Anasaya",
    "Trending Musics": "Anasaya",
    "See All": "Anasaya",
    "Trend Musics": "Anasaya",
    "Trend Tags": "Anasaya",
    "Discover trending musics and become one of the most popular accounts!":
      "Anasaya",
    "Discover trending tags and become one of the most popular accounts!":
      "Anasaya",
    "Trend Sounds and become one of the most popular accounts!": "Anasaya",
    "Discover trending sounds!": "Anasaya",
    "Go to content": "Anasaya",
    "Explore incredible and trendy content ideas for your TikTok videos.":
      "Anasaya",
    // ---------------------------------------
    Get: "Anasaya",
    Continue: "Anasaya",
    Followers: "Anasaya",
    Home: "Anasaya",
    Help: "Anasaya",
    Payment: "Anasaya",
    Success: "Anasaya",
    Transactions: "Anasaya",
    Settings: "Anasaya",
    Earn: "Anasaya",
    Posts: "Anasaya",
    Like: "Anasaya",
    Completed: "Anasaya",
    Pending: "Anasaya",
    Failed: "Anasaya",
    Share: "Anasaya",
    "Privacy & Policy": "Anasaya",
    "Terms & Use": "Anasaya",
    "Search User": "Anasaya",
    "Help center, contact us": "Anasaya",
    "Terms of use": "Anasaya",
    "Terms and Conditions": "Anasaya",
    "Rate us": "Anasaya",
    "Write a review": "Anasaya",
    "Device id not found.": "Anasaya",
    "Go Back": "Anasaya",
    "Purchase successful!": "Anasaya",
    "No Activities Yet": "Anasaya",
    "Once you get likes, followers or views, you will be able to track their status.":
      "Anasaya",
    "Search for Tiktok Account": "Anasaya",
    "Tiktok Username": "Anasaya",
    "Please enter a valid username.": "Anasaya",
    "Search Account": "Anasaya",
    "Linked Accounts": "Anasaya",
    "Link Accounts": "Anasaya",
    "Please add an account to get likes, followers and views.": "Anasaya",
    "No Linked Accounts": "Anasaya",
    "Are you sure?": "Anasaya",
    "Do you want to unlink your account?": "Anasaya",
    "Get Credits": "Anasaya",
    "Private Account": "Anasaya",
    "his account is private. Please make this account public to be able to transactions.":
      "Anasaya",
    "Go back to Home": "Anasaya",
    "Get Likes": "Anasaya",

    "Rate us and get your free credits": "Anasaya",
    "Rate Now": "Anasaya",
    "Search TikTok User": "Anasaya",
    "Download in 4K": "Anasaya",
    "Perfect!": "Anasaya",
    "The download was successful.": "Anasaya",
    "Open in Gallery": "Anasaya",
    "Access Denied!": "Anasaya",
    "You should allow to access gallery.": "Anasaya",
    "Go to Settings": "Anasaya",
  },
};

const i18n = createI18n({
  locale: "en", // set locale
  fallbackLocale: "en", // set fallback locale
  messages,
});

export default i18n;
