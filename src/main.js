import './assets/main.css'
import VueTelInput from 'vue3-tel-input'
import 'vue3-tel-input/dist/vue3-tel-input.css'
import "@/assets/scss/main.scss";
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import { actions } from "@/utils/reactNativeActionFactory";
import globalMixin from "../src/config/globalMixin";
import Loading from "@/components/Loading.vue";
import i18n from "@/utils/i18n";
import { useHumanizeDate } from "@/utils/humanizeMixin.js";
import Base from "@/utils/init";

const VueTelInputOptions = {
    mode: "international",
    onlyCountries: ['NG', 'GH', "GB", "US", "CA"],
}



export const app = createApp(App)

app.config.globalProperties.$reactNative = actions;
app.use(createPinia())
app.mixin(globalMixin);
app.use(router)
app.use(i18n);

app.use( VueTelInput, VueTelInputOptions)
app.component("TLoading", Loading);


app.config.globalProperties.$humanizeDate = useHumanizeDate().humanizeDate;

const base = new Base();
base.init();

router.isReady().then(() => {
    app.mount("#app");
});

