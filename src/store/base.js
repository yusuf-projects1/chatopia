import { app } from "@/main";
import { iosStorePackages } from "@/utils/dummyData";
import { defineStore } from "pinia";
import api from "@/utils/api";
import router from "@/router/index.js";


export const useProdStore = defineStore("prod", {
    state: () => ({
        isAppLoading: true,
        deviceInfo: null,
        userInfo: null,
        silentMode:false,
        account:null,
        storePackages: iosStorePackages,
        showPremium: false,
        isRated: false,
        selectedPoof:null,
        selectedStories:null,
        $reactNative: app.config.globalProperties.$reactNative,
        selectedItem: null,

    }),
    getters: {
        getAccount: (state) => {
            return state.account;
        },

        getPhoneNumber: (state) => {
            return state.account?.data?.info?.phone_number;
        },

        getAccountInfo: (state) => {
            return state.account?.data?.info
        },

        getSilentMode: (state) => {
            return state.silentMode;
        },

        getUser: (state) => {
            return state.userInfo;
        },

        getStatus: (state) => {
          return state.account?.data?.stories || []
        },

        getPoof: (state) => {
            if (state.account?.data?.poof) {
                return state.account?.data?.poof.map((item) => {
                    const lastItem = item.status.items.filter((status) => status.type === "image" || status.type === "video").sort((a, b) => b.timestamp - a.timestamp)[0]?.timestamp;
                    const now = Date.now();
                    const isNew = lastItem && now - lastItem <= 24 * 60 * 60 * 1000;
                    return {
                        ...item,
                        lastItemTimestamp: lastItem,
                        imageCount: item.status.items.filter((status) => status.type === "image").length,
                        videoCount: item.status.items.filter((status) => status.type === "video").length,
                        isPlaying: false,
                        isNew,
                    };
                }).sort((a, b) => b.lastItemTimestamp - a.lastItemTimestamp)
            }
            return [];
        },

        isProd: (state) => {
            return state?.userInfo.is_pro;
        },
        getVersion: (state) => {
            return state.deviceInfo && state.deviceInfo.version;
        },
        isAndroid: (state) => {
            return state.deviceInfo.platform.toLowerCase() === "android";
        },
        requestHeader: (state) => {
            return {
                platform: state.deviceInfo.platform.toLowerCase(),
                device_id: state.deviceInfo.device_id,
            };
        },
        isPremium: (state) => {
            return state.userInfo?.is_premium || false;
        },
        rateStatus: (state) => {
            return state.userInfo?.is_rated || false;
        },
    },
    actions: {
        rate() {
            return api
                .rate({
                    ...this.requestHeader,
                })
                .then((res) => {
                    this.isRated = true;
                    this.userInfo.user.credits = res.data.result.user.credits;
                });
        },
        async connect() {
            this.isAppLoading = true;
            return await api
                .connectUser({ ...this.requestHeader, ...this.deviceInfo })
                .then((res) => {
                    console.log(res.data.result)
                    this.userInfo = res.data.result.user
                    this.account = res.data.result.whatsapp
                    router.push({ name: this.account?.connected ? "home" : "code"})
                    return res.data.result
                })
                .finally(() => {
                    this.isAppLoading = false;
                });
        },

        async connectDevice(payload) {
            //this.isAppLoading = true;
            return await api
                .connectDevice({
                    ...this.requestHeader,
                    ...payload })
                .then((res) => {
                    this.account = res.data.result
                    return res.data.result
                })
                .finally(() => {
                    //this.isAppLoading = false;
                });
        },

        async unlink() {
            this.isAppLoading = true;
            return await api
                .unlink({
                    ...this.requestHeader,
                })
                .then((res) => {
                    this.account = null
                    return res.data.result

                })
                .finally(() => {
                    router.push({ name: "code" });
                    this.isAppLoading = false;
                });

        },

      async  payment(payload) {
            this.isAppLoading = true;
            const requestPayload = {
                ...this.requestHeader,
                ...payload,
            };
            return await api
                .payment(requestPayload)
                .then((res) => {
                    if (res.data.result.is_valid) {
                        this.connect();
                        return true;
                    }
                })
                .finally(() => {
                    this.isAppLoading = false;
                });
        },
        stripe(payload) {
            this.isAppLoading = true;
            const requestPayload = {
                ...this.requestHeader,
                ...payload,
            };
            return api
                .payment(requestPayload)
                .then((res) => {
                    if (res.data.result.is_valid) {
                        this.connect();
                        return true;
                    }
                })
                .finally(() => {
                    this.isAppLoading = false;
                });
        },
        setSelectedPoof(payload) {
            this.selectedPoof = payload;
        },
        setSelectedItem(payload) {
            this.selectedItem = payload;
        },
        setSelectedStories(payload) {
            this.selectedStories = payload;
        },


        goBack() {

            router.go(-1);
        },
       async selectedPoofStories(item) {
            const poofItems = this.getPoof;
            const storiesItems = this.getStatus;

            const statusItemId = item.id;


            const isPoofItemExistInPoof = poofItems.some(poofItem => poofItem.id === statusItemId);

            const poofItemId = item.id;

            const isStatusItemExistInStatus = storiesItems.some(storiesItem => storiesItem.id === poofItemId);

            if (isPoofItemExistInPoof && isStatusItemExistInStatus) {
                const matchedPoofItem = poofItems.find(poofItem => poofItem.id === statusItemId);
                const matchedStoriesItem = storiesItems.find(storiesItem => storiesItem.id === poofItemId);
                this.selectedPoof = matchedPoofItem;
                this.selectedStories = matchedStoriesItem;
                console.log(this.selectedPoof);
                console.log(this.selectedStories);
                console.log('Both items exist in both lists');
               await router.push({ name: 'profile' })
            }

            else if (isPoofItemExistInPoof) {

                const matchedPoofItem = poofItems.find(poofItem => poofItem.id === statusItemId);
                this.selectedPoof = matchedPoofItem;
                this.selectedStories = null;
                console.log(matchedPoofItem);
                console.log(this.selectedStories)
                console.log('Item exists only in poof');
              await  router.push({ name: 'profile' })
            }

            else if (isStatusItemExistInStatus) {

                const matchedStoriesItem = storiesItems.find(storiesItem => storiesItem.id === poofItemId);
                this.selectedPoof = null;
                this.selectedStories = matchedStoriesItem;

                console.log(matchedStoriesItem);
                console.log(this.selectedPoof);
                console.log('Item exists only in status');

               await router.push({ name: 'profile' })
            }
            else {
                console.log('Item does not exist in both lists');
            }
        }







    },
});
