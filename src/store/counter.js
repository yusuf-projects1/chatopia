import { defineStore } from 'pinia';
import jsonData from '../assets/data/chatopia-data.json';

export const useChatopiaStore = defineStore('chatopia', {
  state: () => ({
    data: jsonData
  }),


});
